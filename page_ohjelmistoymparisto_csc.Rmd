---
title: "Ohjelmistoympäristön asennus ja määrittely - CSC:n Poutapilvi"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---



![](http://courses.markuskainu.fi/utur2016/kuviot/ymparisto_1.png)

**Kuvio:** *Kurssin ohjelmistoympäristö*

Ylläolevassa kuvassa on kuvattu ohjelmistoympäristön kaksi eri vaihtoehtoa: 1) ohjelmat CSC:n serverillä, 2) ohjelmat omalla koneella.


<div class="alert alert-dismissible alert-warning">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h1>Kurssimateriaalien "forkkaaminen" Gitlabissa</h1>
  
  <div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Tehdään vain ensimmäisellä kerralla!</strong>
  </div>
  
  <ul>
  <li>1 Siirry osoitteesee: <https://gitlab.com/utur2016/content> ja klikkaa ruudun keskeltä **Fork**</li>
  <li>2 Valitse oma profiilisi, klikkaa **Fork** ja odota hetki</li>
  <li>3 Valitse oikealta hammasratas-ikoni ja sen alta **Members**</li>
  <li>4 kirjoita **People**-kenttään `muuankarski` ja valitse *Markus Kainu* ja anna hänelle *developer*-oikeudet!</li>
  </ul>
  </br>
  <a href="./videot/utur2016_ohjelmistoymparisto1_2.mp4" class="alert-link">Katso video</a>

</div>


<!-- <!--html_preserve--> -->
<!-- <video id="sampleMovie" width="640" height="360" preload controls> -->
<!-- 	<source src="./videot/utur2016_ohjelmistoymparisto1_2.mp4" /> -->
<!-- 	<source src="./videot/utur2016_ohjelmistoymparisto1_2.webm" /> -->
<!-- </video> -->
<!-- <!--/html_preserve--> -->


# Uuden luennon tai kotitehtäväsession alussa


CSC:n Rstudio-istunnot kestävät kerrallaan aina neljä tuntia, jonka jälkeen istunto katoaa. Säilytämme omat koodimme gitlab:ssa, mutta jokaisen **luennon** ja **kotitehtäväkerran** aluksi joudumme yhdistämään/parittamaan uuden r-session ja gitlab:n.

1. Avaa uusi ikkuna/välilehti ja kirjaudu **haka**-tunnuksilla CSC:n **Pouta Blueprints**-palveluun <https://pb.csc.fi/#/>
2. Valitse oikealta otsikon *RStudio Server* alta **Launch new**

# Rstudion & gitlab:n määritykset

<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Tehdään aina kun avataan uusi sessio CSC:n palvelimella</strong>
</div>

## gitin konffaaminen Rstudiossa

Avaa uusi R-skripti ja kopioi alla oleva koodi siihen

```{r, eval=FALSE}
# Vaihda skriptiin omat tietosi!
system('git config --global user.name "Etunimi Sukunimi"')
system('git config --global user.email "utu_tunnus@utu.fi"')
```

Luo [rsa-avain](https://fi.wikipedia.org/wiki/RSA) valitsemalla **Tools** -> **Global Options** -> **Git/SVN** -> *Create RSA Key* (Ei tarvita salasanaa tällä kurssilla!) 

Klikkaa **View public key** ja kopioi se leikepöydälle (Ctrl+c)

- [Katso video](./videot/utur2016_ohjelmistoymparisto1_1.mp4)


## Gitlabin konffaaminen

Siirry selaimella [gitlabiin](https://gitlab.com/users/sign_in) ja 

**session ssh-avaimen lisääminen**

1. klikkaa oikealta **profile settings** -> **SSH keys**.
2. Kopioi isompaan ruutuun julkinen ssh-avain (`Ctrl+v`), hyväksy oletusotsikko ja klikkaa **Add key**


## Rstudion konffaaminen

1. Siirry omaan projektiisi <https://gitlab.com/utu_tunnus/content>
2. Kopioi uuden projektin ssh-osoite ruudun keskeltä joka muotoa: `git@gitlab.com:utu_tunnus/content.git` ja siirry takaisin Rstudioon
3. Klikkaa oikealta **Project: (None)** ja valitse **New Project** -> **Version Control** -> **Git** ja annan kenttään **repository URL** ssh-osoite ja klikkaa **Create Project**
4. Rstudio lataa gitlab-projektista tiedostot Rstudio-projektiin ja voit aloittaa R:n käytön! 
5. Asenna istunnon aluksi aina uusin versio **tidyverse**-paketeista komennolla `install.packages("tidyverse")`

- [Katso video](./videot/utur2016_ohjelmistoymparisto1_3.mp4)


# Kurssimateriaaliin tulleiden muutosten päivittäminen

Kurssin materiaalien sisältö muuttuu koko ajan. Kotitehtävien sisältö EI muutu enää kun kotitehtävä on julkaistu. Saadaksesi uusimmat päivitykset ennen kun aloitat kotitehtävän tekemisen tai luennon alussa avaa aja Rstudiossa seuraavat komennot:

```{r eval=FALSE}
system("git remote add upstream git@gitlab.com:utur2016/content.git") # lisää alkuperäinen upstreamiksi
system("git fetch upstream") # vedä muutokset alkuperäisestä koneellesi upstream/master
# JOS et ole tehnyt TULEVIIN luentoihin/kotitehtäviin muutoksia niin aja
system("git reset --hard") # poista mahdolliset ei-committatut muutokset
system("git merge upstream/master") # yhdistä upstreamista vedetyt muutokset nykyiseen master-branchiin
# JOS taas olet tehnyt muutoksia, niin aja
system("git rebase upstream/master")
```

Mikäli tuntuu siltä että olet sotkenut alkuperäisen materiaalin kokonaan ja haluaisit aloittaa ns. puhtaalta pöydältä voi menetellä seuraavien ohjeiden mukaan, jossa siis a) varmuuskopioidaan projektin tämänhetkinen tila uuteen haaraan (branch) ja b) resetoidaan projektin master-haara identtiseksi upstreamin eli `utur2016/content`-projektin kanssa.

```{r eval=FALSE}
system('git commit -am "committaan vanhat sotkut varmuuden vuoksi"') # nykyisten muutosten committaus
system("git branch vanha_master") # nykyinen projektin haaraan vanha_master
system("git checkout vanha_master") # Vaihdetaan tähän uuteen haaraan
system("git push origin vanha_master") # siirretään se gitlabiin turvaan varmuuden vuoksi
system("git remote add upstream git@gitlab.com:utur2016/content.git") # lisää alkuperäinen upstreamiksi varmuuden vuoksi
system("git fetch upstream") # vedä muutokset alkuperäisestä koneellesi upstream/master
system("git checkout master") # varmistetaan että ollaan masterissa
system("git reset --hard upstream/master") # resetoidaan upstreamin kanssa
system("git push origin master --force") # korvataan gitlab:ssa entinen master tällä uudella 
```



# Kotitehtävien tekeminen ja muutosten tallentaminen Gitlabiin session päätteksi

**Metodi 1 - IDE:n avulla**

1. mene Rstudiossa **git** välilehdelle ja 
    1. klikkaa tiedostot joiden muutokset haluat saada talteen ja klikkaa **Commit**
    2. kirjoita ruutuun **muutoksia kuvaava viesti sekä syy muutoksille** ja klikkaa **Commit*
    3. Sitten klikkaa ensin **Pull** ja anna ssh-salasanasi 
    4. Sitten klikkaa **Push**
4. Sulje Rstudio tai aloita jo kotitehtävät!

**Metodi 2 - koodilla**

```{r eval=FALSE}
system('git commit -am "tein kotitehtäviä ja annan tähän runsaan kuvauksen siitä mitä tein, mikä onnistui ja mikä oli vaikeaa"')
system("git pull origin master")
system("git push origin master")
```



