---
title: "LOPPUTYÖ"
author: "Jannika Haavisto 510705"
date: "Päivitetty: `r Sys.time()`"
output:
  html_document:
    toc: true
    number_sections: true
    theme: united
    code_folding: hide
    toc_float:
      collapsed: false
      smooth_scroll: false
---


```{r setup, include=FALSE}
library(knitr)
library(tidyverse)

opts_chunk$set(list(echo=TRUE, # kaikkiin koodichunkkeihin tulee koodia. Mieti aina onko tarpeen, mutta html-dokkarissa voit käyttää YAML:ssa code_folding: hide
                    eval=TRUE, # kaikki koodikimpaleet arvioidaan
                    cache=FALSE, # koodikimpaleista ei luoda välimuistia. Raskaammissa projekteissa tätä kannattaa käyttää
                    warning=FALSE, # funktioiden varoituksia ei printata outputtiin 
                    message=FALSE, # funktioiden viestejä ei printata outputtiiin
                    fig.width=8, # kuvien oletuskorkeus
                    fig.height=8) # kuvien oletusleveys 
               )
# joitain optioita
options(scipen=999) # outputteihin tavanomaiset luvut tieteellisen merkinnäin sijaan
```

# Marjojen tuotanto

Kuinka monta tonnia marjoja Suomessa, Ruotsissa, Norjassa ja Tanskassa tuotettiin vuosina 2010-2014?
Tutkitut marjat sisältävät mansikat, mustikat, viinimarjat, karpalot, karviaismarjat sekä vadelmat.



``` {r}
# kääritään datan hakeminen ehtoon
if (!file.exists("./datakansio/fao.RDS")){
  dir.create("./datakansio")
  dir.create("./aineisto/kuviot", showWarnings = TRUE, recursive = TRUE)
  # fao <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS")))
  # download.file(url = "http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS" ,destfile = "./aineisto/fao.csv")
  # Ladataan alkuperäinen aineisto
  download.file("http://fenixservices.fao.org/faostat/static/bulkdownloads/Production_Crops_E_All_Data_(Norm).zip", destfile = "./datakansio/Production_Crops_E_All_Data_(Norm).zip")
  unzip("./datakansio/Production_Crops_E_All_Data_(Norm).zip",exdir = "./datakansio")
  fao <- read_csv("./datakansio/Production_Crops_E_All_Data_(Norm).csv")
  saveRDS(fao, file="./datakansio/fao.RDS")
}
fao <- readRDS(file="./datakansio/fao.RDS")
```





#Datan tarkastelu ja marjat-muuttujan luominen

```{r}
library(tidyr)
library(dplyr)
d <- fao
#Kuinka paljon Suomessa, Ruotsissa, Norjassa ja Tanskassa tuotettiin marjoja vuosina 2010-2014
#luodaan muuttuja "marjat"
d %>% filter(Country %in% c("Finland", "Sweden", "Norway", "Denmark"),
             Element %in% "Production",
             Item %in% c("Blueberries", "Cranberries", "Currants", "Gooseberries", "Raspberries", "Strawberries"),
             Unit %in% "tonnes",
             Year %in% 2010:2014) %>%
  select(Year,Value, Country, Item) -> marjat
```

# Tolppakuvioiden piirtäminen

Kuvio 1. "Marjojen tuotanto kaikissa tutkituissa maissa"
Kuvio 2. "Marjojen tuotanto maittain"

Yhteensä tutkituissa maissa tuotettiin mansikoita vuosien 2010-2014 välillä yli 15 tonnia. Seuraavaksi eniten tuotettiin viinimarjoja (yli 6 tonnia). Tutkituissa maissa tutkitulla ajanjaksolla mustikoita ja vadelmia tuotettiin keskenään käytännössä saman verran ja karviaismarjoja tuotettiin vähiten.

Maittainen vertailu osoittaa viinimarjojen suuren tuotantomäärän johtuvan Tanskasta, sillä muissa tutkituissa maissa viinimarjoja ei tuotettu tutkitulla ajanjaksolla lähes ollenkaan. Tanska on myös ainut maa, jossa mansikat eivät olleet tuotetuin marja. Ruotsissa ja Suomessa tuotettiin mansikoita saman verran kuin Tanskassa viinimarjoja (n. 5000 tonnia/maa). Norjassa tuotettiin marjoja selkeästi vähemmän kuin muissa tutkituissa maissa, mutta sielläkin mansikka oli suosituin marja. Kuvioiden perusteella mustikoita tuotettiin käytännössä vain ruotsissa. Karviaismarjoja ei tuotettu tutkituissa maissa juuri lainkaan. 

```{r}

#Piirretään kuviot
library(ggplot2)
#tolppakuviot
ggplot(marjat, aes(x=Item,y=Value)) + geom_bar(stat="identity")
ggplot(marjat, aes(x=Item,y=Value)) + geom_bar(stat="identity") +facet_wrap(~Country)
```

# Maittaisten viivakuvioiden piirtäminen

Viivakuvio 1. "Marjojen tuotanto Suomessa vuosina 2010-2014"
Viivakuvio 2. "Marjojen tuotanto Ruotsissa vuosina 2010-2014"
Viivakuvio 3. "Marjojen tuotanto Norjassa vuosina 2010-2014"
Viivakuvio 4. "Marjojen tuotanto Tanskassa vuosina 2010-2014"

Viivakuvio 1. osoittaa, että tutkitulla ajanjaksolla Suomessa ei ole ollut karviaismarjojen tuotantoa ja vadelmien tuotanto on pysynyt hyvin matalalla ja tasaisella tasolla. Myös viinimarjojen tuotanto on pysynyt matalana. Mansikoiden tuotanto nousi suhteellisen tasaisesti vuoteen 2012 asti, jonka jälkeen se on kääntynyt laskuun.

Viivakuvio 2. osoittaa, että Ruotsissa ei tutkitulla ajanjaksolla tuotettu juuri lainkaan viinimarjoja eikä vadelmia. Mustikoiden tuotanto on pysynyt tasaisesti hieman yli kahdessa ja puolessa tonnissa. Mansikoiden tuotanto nousi suhteellisen jyrkäsi saavuttaen huippunsa vunna 2012 ja kääntyen sen jälkeen jyrkkään laskuun.

Viivakuvio 3. osoittaa, että myöskään Norjassa ei tuotettu tutkitulla ajanjaksolla oikeastaan lainkaan karviaismarjoja tai mustikoita. Viinimarjojen tuotanto on pysynyt tasaisesti matalana. Vadelmien tuotantomäärä on pysynyt melko tasaisesti hieman alle kahdessa ja puolessa tonnissa. Mansikoiden tuotanto lähti matalan laskun jälkeen nousuun vuonna 2011 saavuttaen huippunsa vuonna 2012 ja kääntyen sen jälkeen jyrkkään laskuun. 

Viivakuvio 4. osoittaa, että tutkitulla ajanjaksolla Tanskassa vadelmia ja mustikoita ei ole tuotetta lainkaan, ja karviasmarjoja on tuotettu hyvin vähän. Mansikoiden tuotantomäärä nousi vuoteen 2011 asti ja kääntyi sen jälkeen hyvin loivaan laskuun. Viinimarjojen tuotanto on pysynyt jatkuvasti korkeana ja hyvin tasaisena, kääntyen loivaan nousuun vuonna 2012.

```{r}
#jokaiselle maalle viivakuvio
#Suomi
d %>% filter(Country %in% c("Finland"),
             Element %in% "Production",
             Item %in% c("Blueberries", "Cranberries", "Currants", "Gooseberries", "Raspberries", "Strawberries"),
             Unit %in% "tonnes",
             Year %in% 2010:2014) %>%
  select(Year,Value, Country, Item) -> marjat_suomi
ggplot(marjat_suomi, aes(x=Year,y=Value,color=Item)) + geom_line()
#Ruotsi
d %>% filter(Country %in% "Sweden",
             Element %in% "Production",
             Item %in% c("Blueberries", "Cranberries", "Currants", "Gooseberries", "Raspberries", "Strawberries"),
             Unit %in% "tonnes",
             Year %in% 2010:2014) %>%
  select(Year,Value, Country, Item) -> marjat_ruotsi
ggplot(marjat_ruotsi, aes(x=Year,y=Value,color=Item)) + geom_line()
#Norja
d %>% filter(Country %in% "Norway",
             Element %in% "Production",
             Item %in% c("Blueberries", "Cranberries", "Currants", "Gooseberries", "Raspberries", "Strawberries"),
             Unit %in% "tonnes",
             Year %in% 2010:2014) %>%
  select(Year,Value, Country, Item) -> marjat_norja
ggplot(marjat_norja, aes(x=Year,y=Value,color=Item)) + geom_line()
#Tanska
d %>% filter(Country %in% "Denmark",
             Element %in% "Production",
             Item %in% c("Blueberries", "Cranberries", "Currants", "Gooseberries", "Raspberries", "Strawberries"),
             Unit %in% "tonnes",
             Year %in% 2010:2014) %>%
  select(Year,Value, Country, Item) -> marjat_tanska
ggplot(marjat_tanska, aes(x=Year,y=Value,color=Item)) + 
  geom_line() + geom_point() +
  scale_y_log10() +
  theme_light() +
  labs(title="Marjojen tuotanto Tanskassa vuosina 2010-2014",
       subtitle="
- Huomaa että y-akselissa on logaritminen asteikko! 
- Marjojen viivat voisivat olla marjojen väriset! 
- Sen voisi tehdä scale_color_manual(values=c('red','blue',jne))",
       caption="Data: faostat3.fao.org",
       x=NULL,
       y="Vuosituotanto jotain yksikkö",
       color="marjalajikkeet")
```

# Kuvioiden tallentaminen

Luotujen kuvioiden tallentaminen mahdollista myöhempää käyttöä varten.

```{r}
dir.create("./aineisto/kuviot")
Suomi <-ggplot(marjat_suomi, aes(x=Year,y=Value,color=Item)) + geom_line()
ggsave(plot=Suomi, filename = "Suomi.png", path="./aineisto/kuviot") 
Ruotsi <-ggplot(marjat_ruotsi, aes(x=Year,y=Value,color=Item)) + geom_line()
ggsave(plot=Ruotsi, filename = "Ruotsi.png", path="./aineisto/kuviot") 
Norja <-ggplot(marjat_norja, aes(x=Year,y=Value,color=Item)) + geom_line()
ggsave(plot=Norja, filename = "Norja.png", path="./aineisto/kuviot") 
Tanska <-ggplot(marjat_tanska, aes(x=Year,y=Value,color=Item)) + geom_line()
ggsave(plot=Tanska, filename = "Tanska.png", path="./aineisto/kuviot") 
Tolppa1 <-ggplot(marjat, aes(x=Item,y=Value)) + geom_bar(stat="identity")
ggsave(plot=Tolppa1, filename = "Tolppa1.png", path="./aineisto/kuviot") 
Tolpat2 <-ggplot(marjat, aes(x=Item,y=Value)) + geom_bar(stat="identity") +facet_wrap(~Country)
ggsave(plot=Tolpat2, filename = "Tolpat2.png", path="./aineisto/kuviot")
```


# Tallennettujen kuvien esittäminen

Voit myös tehdä näin

![Viivakuvio 1. Marjojen tuotanto Suomessa vuosina 2010-2014](./aineisto/kuviot/Suomi.png)
